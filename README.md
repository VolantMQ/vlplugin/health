# Overview
[![pipeline status](https://gitlab.com/volantmq/vlplugin/health/badges/master/pipeline.svg)](https://gitlab.com/volantmq/vlplugin/health/commits/master)

## Config
Docker image of VolantMQ service comes with `health` plugin
To enable plugin add `health` value to list of enabled plugins. In config section listening port and path can be specified
```yaml
plugins:
  enabled:
    - health
  config:
    health:
      - backend: health
        config:
          port: 8080                # if not provided default port from server config is used
          path: /health             # path prefix for endpoints below. if empty default /health is used 
          livenessEndpoint: live    # token path for liveness checks. in this example it available on http://localhost:8080/health/live
          readinessEndpoint: ready  # token path for readiness checks. in this example it available on http://localhost:8080/health/ready
```
