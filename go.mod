module gitlab.com/VolantMQ/vlplugin/health

go 1.13

require (
	github.com/VolantMQ/vlapi v0.5.6
	github.com/troian/healthcheck v0.1.3
	gopkg.in/yaml.v3 v3.0.0-20200121175148-a6ecf24a6d71
)
